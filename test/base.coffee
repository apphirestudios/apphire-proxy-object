"use strict"
Proxy = require '../lib'
async = require 'co'
assert = require 'apphire-helpers'

module.exports = ->
  describe "Proxies", ->
    original = proxied = undefined
    beforeEach ->
      original =
        u: undefined
        s: 10
        sub:
          t: 12
        arr: ['12', '48']
        date: new Date()
        func: ->
          return 12
      proxied = new Proxy(original)

    describe "init", ->
      it "regular", ->
        o =
          s: 10
        p = new Proxy(o)
        p.s.should.equal o.s

      it 'getTransformers', ->
        class P extends Proxy
          config:
            getTransformers: [ (value, path)-> if value is 10 then 11]
        o =
          s: 10
        p = new P(o)
        p.s.should.equal 11

      it 'setTransformers', ->
        class P extends Proxy
          config:
            setTransformers: [ (newVal, oldVal, path, parent, propName)->
              newVal.should.equal 5
              oldVal.should.equal 10
              path.should.equal 's.i'
              parent.i.should.equal p.s.i
              propName.should.equal 'i'
              if newVal is 5 then 11
            ]
        o =
          s:
            i: 10
        p = new P(o)
        p.s.i = 5
        p.s.i.should.equal 11

      it "in place", ->
        cfg =
          getTransformers: [ (value, path)-> if value is 10 then 11]
        o =
          s: 10
        Proxy.proxify(o, cfg)
        o.s.should.equal 11

      it "extend", ->
        cfg =
          getTransformers: [ (value, path)-> if value is 10 then 11]
        o =
          o: 9
        e =
          s: 10
        Proxy.extend(o, e, cfg)
        o.s.should.equal 11
        o.o.should.equal 9

    describe "get", ->
      it "Ordinal", ->
        proxied.s.should.equal original.s
      it "Undefined", ->
        expect(proxied.u).to.be.undefined
      it "Inner", ->
        proxied.sub.t.should.equal original.sub.t
      it "Date", ->
        proxied.date.should.equal original.date
      it "Array", ->
        proxied.arr[0].should.equal original.arr[0]

    describe "set", ->
      it "Sealing", ->
        expect( -> proxied.s2 = 'New').to.throw /add property s2, object is not extensible/
        expect( -> proxied.sub.s2 = 'New').to.throw /add property s2, object is not extensible/
        expect( -> proxied.sub.t.s2 = 'New').to.throw /Cannot create property/

      it "Ordinal", ->
        proxied.s = 'New'
        proxied.s.should.equal 'New'
        original.s.should.equal 'New'
        proxied.toString().should.equal original.toString()


      it "Inner", ->
        proxied.sub.t = 'New'
        proxied.sub.t.should.equal 'New'
        original.sub.t.should.equal 'New'

      it "Object", ->
        proxied.sub = 'New'
        proxied.sub.should.equal 'New'
        original.sub.should.equal 'New'

      it "Object - deep", ->
        proxied.sub.t = {t2: 'New'}
        proxied.sub.t.t2.should.equal 'New'
        original.sub.t.t2.should.equal 'New'


      it "Array - set whole", ->
        proxied.arr = ['1', '2']
        proxied.arr[0].should.equal '1'
        proxied.arr[1].should.equal '2'
        original.arr[0].should.equal '1'
        original.arr[1].should.equal '2'

      it "Array - set existing elem", ->
        proxied.arr[0] = '1'
        proxied.arr[0].should.equal '1'
        proxied.arr[1].should.equal '48'
        original.arr[0].should.equal '1'
        original.arr[1].should.equal '48'


      it "Array - push", ->
        proxied.arr.push('56')
        #proxied.arr[2].should.equal '56'
        original.arr[2].should.equal '56'

      it "Array - push object", ->
        proxied.arr.push({sub: '56'})
        proxied.arr[2].sub.should.equal '56'
        original.arr[2].sub.should.equal '56'

      it "Array - pop", ->
        proxied.arr.push('56')
        elem = proxied.arr.pop()
        elem.should.equal '56'


    setAttr = (obj)->
      if not assert.isPrimitive obj
        for k,v of obj
          obj[k] = v
          setAttr obj[k]
    afterEach ->
      setAttr(proxied)
      JSON.stringify(proxied).should.equal JSON.stringify(original)




"use strict"
assert = require 'apphire-helpers'

defProp = (obj, name, value)->
  Object.defineProperty obj, name,
    configurable: true
    enumerable: false
    value: value

arrayMethods = ['pop', 'reverse', 'splice', 'shift', 'sort', 'unshift']


defineProxiedProperty = (obj, propName, proxied, config, path)->
  path ?= propName
  getProperty = ->
    target = proxied[propName]
    if assert.isPrimitive(target)
      ret = target
    else
      if assert.isPlainObject(target)
        ret = {}
      else if assert.isArray(target)
        ret = []
        pushOrig = ret.push.bind ret
        defProp ret, 'push', (elem)->
          if config?.setTransformers
            for s in config.setTransformers
              elem = s(elem, undefined, path + '.' + target.length, obj, target.length)
          res = pushOrig(elem)
          target.push elem
          res

        arrayMethods.map (method)->
          original = ret[method].bind ret
          defProp ret, method, (elem)->
            res = original()
            target[method].apply target, arguments
            res
      else if assert.isFunction(target)
        ret = -> target.apply @, arguments
      else if assert.isObject(target)
        ret = {}
        ret.constructor = target.constructor
      for k, v of target
        defineProxiedProperty ret, k, target, config, (path + '.' + k)

    if config?.getTransformers
      for g in config.getTransformers
        ret = g(ret, path, obj, propName)

    if not assert.isArray(ret)
      Object.seal ret
    return ret


  setProperty = (val)->
    oldVal = proxied[propName]
    if config?.setTransformers
      for s in config.setTransformers
        val = s(val, proxied[propName], path, obj, propName)
    proxied[propName] = val
    if config?.setListeners
      for l in config.setListeners
        l(val, oldVal, path, obj, propName)


  Object.defineProperty obj, propName,
    configurable: true
    enumerable: true
    get: getProperty.bind @
    set: setProperty.bind @

class Proxy
  @extend: (obj, attrs, config)->
    for propName, value of attrs
      defineProxiedProperty obj, propName, attrs, config
    return obj

  @proxify: (obj, config)->
    proxied = {}
    proxy = obj
    for k, v of obj
      proxied[k] = v
      delete obj[k]
    for propName, value of proxied
      defineProxiedProperty proxy, propName, proxied, config

  constructor: (proxied, config)->
    for propName, value of proxied
      defineProxiedProperty @, propName, proxied, config
    delete @proxyfy
    Object.seal @
    return @

module.exports = Proxy
